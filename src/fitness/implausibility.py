
import os
import numpy as np
import pandas as pd


class Implausibility:

    def __init__(self, targets, data_mean, msv, number_years_i=26, range_percentage=0.1):
        self._targets = targets
        self._data_mean = data_mean
        self._mean_s_var = msv  # mean sample variance, dimensions: number_years x 6 matrix
        self._n_years = number_years_i
        self._range_percentage = range_percentage
        self._compute = True

    def get_fitness(self):
        if self._compute:
            self._determine_implausibility()
            self._compute = False
        return {'fitness_all': self.fitness_all, 'fitness_male': self.fitness_male,
                'fitness_female': self.fitness_female}

    def get_fitness_all(self):
        if self._compute:
            self._determine_implausibility()
            self._compute = False
        return self.fitness_all

    def get_fitness_male(self):
        if self._compute:
            self._determine_implausibility()
            self._compute = False
        return self.fitness_male

    def get_fitness_female(self):
        if self._compute:
            self._determine_implausibility()
            self._compute = False
        return self.fitness_female

    def get_ip_metric(self):
        if self._compute:
            self._determine_implausibility()
            self._compute = False
        return self._ip_metric

    def _determine_implausibility(self):
        # process targets
        mean_col_str = ['PREV_SEX1P', 'PREV_SEX2P', 'FREQ_SEX1P', 'FREQ_SEX2P', 'QUANT_SEX1P', 'QUANT_SEX2P']
        se_col_str = ['PREV_SEX1SE', 'PREV_SEX2SE', 'FREQ_SEX1SE', 'FREQ_SEX2SE', 'QUANT_SEX1SE', 'QUANT_SEX2SE']
        targets_m = self._targets[mean_col_str].iloc[:self._n_years].to_numpy()  # targets_m is number_years x 6 matrix
        targets_s = self._targets[se_col_str].iloc[:self._n_years].to_numpy()  # targets_s is number_years x 6 matrix

        # rangeMax
        a = np.ones((self._n_years, 6))
        a[:, 2] = 30
        a[:, 3] = 30
        a[:, 4] = 40
        a[:, 5] = 40
        range_d = (self._range_percentage * a) ** 2  # a is number_years x 6 matrix

        # denominator
        denominator = (targets_s ** 2 + range_d + self._mean_s_var) ** (1 / 2)  # denominator is number_years x 6 matrix

        # implausibility equation
        self._ip_metric = np.abs(self._data_mean - targets_m) / denominator
        # ip_metric_mean = np.nanmean(self._ip_metric, axis=0)  # Determine the mean along the time for each output

        # self.fitness_all = np.nanmax(ip_metric_mean)
        # self.fitness_male = np.nanmax(ip_metric_mean[[0, 2, 4]])
        # self.fitness_female = np.nanmax(ip_metric_mean[[1, 3, 5]])

        self.fitness_all = np.nanmax(self._ip_metric)
        self.fitness_male = np.nanmax(self._ip_metric[:, [0, 2, 4]])
        self.fitness_female = np.nanmax(self._ip_metric[:, [1, 3, 5]])

    @staticmethod
    def read_annual_data(number_years):

        _filename = './norms/outputs/annual_data.csv'
        df = pd.read_csv(_filename, encoding='ISO-8859-1')
        df = df.assign(
            PREV_SEX1P=lambda df_: np.where(df_['Male'] > 0, df_['12MonthDrinkersMale'] / df_['Male'], 0),
            PREV_SEX2P=lambda df_: np.where(df_['Female'] > 0, df_['12MonthDrinkersFemale'] / df_['Female'], 0),

            # average frequency
            FREQ_SEX1P=lambda df_: np.where(df_['12MonthDrinkersMale'] > 0,
                                            df_['FreqMale'] / df_['12MonthDrinkersMale'], 0),
            FREQ_SEX2P=lambda df_: np.where(df_['12MonthDrinkersFemale'] > 0,
                                            df_['FreqFemale'] / df_['12MonthDrinkersFemale'], 0),

            # average quantity among drinkers in grams per day
            QUANT_SEX1P=lambda df_: np.where(df_['12MonthDrinkersMale'] > 0,
                                             df_['QuantMale'] * 14 / df_['12MonthDrinkersMale'], 0),
            QUANT_SEX2P=lambda df_: np.where(df_['12MonthDrinkersFemale'] > 0,
                                             df_['QuantFemale'] * 14 / df_['12MonthDrinkersFemale'], 0)
        )

        mean_col_str = ['PREV_SEX1P', 'PREV_SEX2P', 'FREQ_SEX1P', 'FREQ_SEX2P', 'QUANT_SEX1P', 'QUANT_SEX2P']
        df = df[mean_col_str].iloc[:number_years]
        return df.to_numpy()

    @staticmethod
    def get_mean_sample_variance(msv_folder):
        msv_file = os.path.join(msv_folder, 'mean_sample_variance.csv')
        df = pd.read_csv(msv_file, encoding='ISO-8859-1')
        dn = df.to_numpy()
        return dn

    @staticmethod
    def read_targets(target_folder):
        target_file = os.path.join(target_folder, 'BRFSS_Targets_reweighted_NY.csv')
        df = pd.read_csv(target_file, encoding='ISO-8859-1')
        return df
