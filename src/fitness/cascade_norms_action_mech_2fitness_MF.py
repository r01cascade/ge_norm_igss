from fitness.base_ff_classes.base_ff import base_ff
import hashlib # to create hash from ind string
from algorithm.parameters import params # for folder name
import os # for cd class, path exist
from distutils.dir_util import mkpath, copy_tree, remove_tree # create, copy, delete temp Model folder
import subprocess # to execute shell script

from collections import OrderedDict
from datetime import datetime

class cd:
    """Context manager for changing the current working directory"""
    def __init__(self, newPath):
        self.newPath = os.path.expanduser(newPath)

    def __enter__(self):
        self.savedPath = os.getcwd()
        os.chdir(self.newPath)

    def __exit__(self, etype, value, traceback):
        os.chdir(self.savedPath)

class cascade_norms_action_mech_2fitness_MF(base_ff):
    """
    A single fitness class that generates
    2 fitness values for multiobjective optimisation:
    prev+freq+quant male, prev+freq+quant female
    """
    maximise = False
    multi_objective = True

    def __init__(self):
        # Initialise base fitness function class.
        super().__init__()

        # Set list of individual fitness functions.
        self.num_obj = 2
        maximise = False
        dummyfit = base_ff()
        self.fitness_functions = [dummyfit, dummyfit]
        self.default_fitness = [float('nan'), float('nan')]

    def replace_norms_code(self, phenotype, folder_path, replication_index):
        # replace follows this order because there are overlaps in variable names
        dict = OrderedDict([
            ('Autonomy', 'mAutonomy'), 
            ('LogOddsDesire', 'logOddsDesire'),
            ('LogOddsInjNorm', 'logOddsInjNorm'),
            ('LogOddsDesNorm', 'logOddsDesNorm'),
            ('0.333','(1.0/3.0)')
        ])
        strs = phenotype
        for bnf_text, c_variable in dict.items():
            strs = strs.replace(bnf_text, c_variable)
        strs = strs.split(' ')
        str_log_odds_attitude = strs[0].replace('LogOddsAttitude=','return ')+'\n'
        str_log_odds_norms = strs[1].replace('LogOddsNorm=','return ')+'\n'
        str_beta_attitude = strs[2]
        str_beta_norm = strs[3]

        str_automaticity_low = strs[4][strs[4].find('=')+1 : -1]
        str_automaticity_med = strs[5][strs[5].find('=')+1 : -1]
        str_automaticity_high = strs[6][strs[6].find('=')+1 : -1]

        str_autonomy_male_low = strs[7][strs[7].find('=')+1 : -1]
        str_autonomy_male_med = strs[8][strs[8].find('=')+1 : -1]
        str_autonomy_male_high = strs[9][strs[9].find('=')+1 : -1]

        str_autonomy_female_low = strs[10][strs[10].find('=')+1 : -1]
        str_autonomy_female_med = strs[11][strs[11].find('=')+1 : -1]
        str_autonomy_female_high = strs[12][strs[12].find('=')+1 : -1]

        # use R script to update automaticity & autonomy of agents
        with cd(folder_path):
            cmd_automaticity = 'Rscript updateAutomaticity.R ' + str_automaticity_low + ' ' + str_automaticity_med + ' ' + str_automaticity_high;
            process = subprocess.Popen(cmd_automaticity, shell=True, stdout=subprocess.PIPE)
            process.wait()

            cmd_automaticity = 'Rscript updateAutonomy.R ' + str_autonomy_male_low + ' ' + str_autonomy_male_med + ' ' + str_autonomy_male_high + ' ' + str_autonomy_female_low + ' ' + str_autonomy_female_med + ' ' + str_autonomy_female_high;
            process = subprocess.Popen(cmd_automaticity, shell=True, stdout=subprocess.PIPE)
            process.wait()

        # replace parameters in model.props
        model_props_file = folder_path + '/norms/props/model.props'
        with open(model_props_file) as f:
            props = f.read()
            props.replace('random.seed=93751',"random.seed=9375"+str(replication_index+1))
            props.replace('stop.at=10950','stop.at=7300') #GE works with 20 years only
            props = props.replace('beta.attitude=0.759',str_beta_attitude[:str_beta_attitude.find(';')])
            props = props.replace('beta.norm=0.842',str_beta_norm[:str_beta_norm.find(';')])
        with open(model_props_file, 'w') as f:
            f.write(props)

        # check theory-related conditions for each term, return false if failed
        if ('logOddsDesire' not in str_log_odds_attitude) or (('logOddsInjNorm' not in str_log_odds_norms) and ('logOddsDesNorm' not in str_log_odds_norms)):
            return False
        else:
            # insert lines of codes to the simulation code
            template_file = folder_path + '/norms/src/NormTheory.template.cpp'
            new_file = folder_path + '/norms/src/NormTheory.cpp'
            with open(template_file) as f:
                new_source = f.read()
                new_source = new_source.replace('//GE-ATTITUDE',str_log_odds_attitude)
                new_source = new_source.replace('//GE-NORM',str_log_odds_norms)
            with open(new_file, 'w') as f:
                f.write(new_source)
            return True

    def evaluate(self, ind, **kwargs):
        total_replication = params['SIMULATION_REPLICATION']
        fitness_Male = 0
        fitness_Female = 0
        for replication_index in range(total_replication):
            # Generate hash with md5 from phenotype
            hash_object = hashlib.md5((ind.phenotype+str(replication_index)+str(datetime.now())).encode())
            folder_path = hash_object.hexdigest()

            # Create a model folder
            folder_path = params['MODELS_FOLDER'] + '/' + folder_path
            mkpath(folder_path)

            # Copy simulation into a model folder
            copy_tree(params['SIMULATION_FOLDER'], folder_path)

            # Edit norms theory with phenotype
            conditions_flag = self.replace_norms_code(ind.phenotype, folder_path, replication_index)

            # Run the simulation, compare to targets, to get fitness
            if not conditions_flag:
                fitness_Male = base_ff.default_fitness # bad fitness
                fitness_Female = base_ff.default_fitness # bad fitness
            else:
                # use cd class (for context manager) to execute the simulation
                # outside the context manager we are back wherever we started.
                with cd(folder_path):
                    # Run a shell script: run RepastHPC simulation, and R script for fitness, wait
                    process = subprocess.Popen('bash compileThenRun.sh', shell=True, stdout=subprocess.PIPE)
                    process.wait()
                    
                    # Read fitness from file
                    if process.returncode != 0: #if error
                        fitness_Male = base_ff.default_fitness # bad fitness
                        fitness_Female = base_ff.default_fitness # bad fitness

                        #return (stop evaluation)
                        remove_tree(folder_path)
                        fitness = [fitness_Male, fitness_Female, ind.nodes]
                        return fitness
                    else:
                        # Read fitness from file
                        f = open("fitness.out", "r")
                        fitness_Male += float(f.readline())
                        fitness_Female += float(f.readline())
            
            # Delete a model folder
            remove_tree(folder_path)

        # Return 2 fitness
        fitness = [fitness_Male/total_replication, fitness_Female/total_replication]
        return fitness

    @staticmethod
    def value(fitness_vector, objective_index):
        """
        This is a static method required by NSGA-II for sorting populations
        based on a given fitness function, or for returning a given index of a
        population based on a given fitness function.

        :param fitness_vector: A vector/list of fitnesses.
        :param objective_index: The index of the desired fitness.
        :return: The fitness at the objective index of the fitness vecror.
        """

        if not isinstance(fitness_vector, list):
            return float("inf")

        return fitness_vector[objective_index]
