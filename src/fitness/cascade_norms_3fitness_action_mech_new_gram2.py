
import glob, os

from fitness.base_ff_classes.base_ff import base_ff
import hashlib  # to create hash from ind string
from algorithm.parameters import params # for folder name
import os  # for cd class, path exist
from distutils.dir_util import mkpath, copy_tree  # create, copy, delete temp Model folder
from distutils.file_util import copy_file
from shutil import rmtree
import subprocess  # to execute shell script
import re
import multiprocessing

from random import randint
import numpy as np
import pandas as pd
from scipy.stats import beta
from scipy.stats import truncnorm

from collections import OrderedDict
from datetime import datetime
from fitness.implausibility import Implausibility

plock = multiprocessing.Lock()
number_years = 26


class cascade_norms_3fitness_action_mech_new_gram2(base_ff):
    """
    A single fitness class that generates
    3 fitness values for multiobjective optimisation:
    prev+freq+quant male, prev+freq+quant female, tree size
    """
    maximise = False
    multi_objective = True

    def __init__(self):
        # Initialise base fitness function class.
        super().__init__()

        # Set list of individual fitness functions.
        self.num_obj = 3
        maximise = False
        dummyfit = base_ff()
        self.fitness_functions = [dummyfit, dummyfit, dummyfit]
        self.default_fitness = [float('nan'), float('nan'), float('nan')]

    def evaluate(self, ind, **kwargs):

        # needed to ensure that each worker has its own random numbers
        # rinteger = randint(0, 1000000)  # get a random number from the standard Python RNG
        # rng = np.random.default_rng(params['RANDOM_SEED'] + ind.name + rinteger)

        total_replication = params['SIMULATION_REPLICATION']
        annual_data_np_list = []
        log_odds_intention = ''
        gender_option = ''
        calibration_parameters = ''
        for replication_index in range(total_replication):
            # Generate hash with md5 from phenotype
            hash_object = hashlib.md5((ind.phenotype+str(replication_index)+str(datetime.now())).encode())
            folder_path = hash_object.hexdigest()

            # Create a model folder
            folder_path = params['MODELS_FOLDER'] + '/' + folder_path
            mkpath(folder_path)

            # Copy simulation into a model folder
            copy_tree(params['SIMULATION_FOLDER'], folder_path)

            # Edit norms theory with phenotype
            log_odds_intention = replace_norms_code(ind.phenotype, folder_path)

            # Run the simulation, compare to targets, to get fitness

            # Load "calibration" parameters based on the gender option
            strs = ind.phenotype.split(';')
            gender_option = strs[1][strs[1].find('=')+len('='):]
            calibration_parameters = get_calibration_parameters(folder_path, gender_option)
            if gender_option == 'all':
                copy_file(folder_path + '/agents.rank1.all.csv', folder_path + '/norms/props/agents.rank1.csv')
                copy_file(folder_path + '/spawn.agents.rank1.all.csv', folder_path + '/norms/props/spawn.agents.rank1.csv')
            elif gender_option == 'male':
                copy_file(folder_path + '/agents.rank1.male.csv', folder_path + '/norms/props/agents.rank1.csv')
                copy_file(folder_path + '/spawn.agents.rank1.male.csv', folder_path + '/norms/props/spawn.agents.rank1.csv')
            else:
                copy_file(folder_path + '/agents.rank1.female.csv', folder_path + '/norms/props/agents.rank1.csv')
                copy_file(folder_path + '/spawn.agents.rank1.female.csv', folder_path + '/norms/props/spawn.agents.rank1.csv')

            # update model.props parameters
            update_model_prop_parameters(folder_path, calibration_parameters, replication_index)

            # use cd class (for context manager) to execute the simulation
            # outside the context manager we are back wherever we started.
            with cd(folder_path):
                # Run a shell script: run RepastHPC simulation
                process = subprocess.Popen('bash compileThenRun.sh', shell=True, stdout=subprocess.PIPE)
                process.wait()

                # Save annual data
                if process.returncode != 0:  # if error
                    fitness_male = base_ff.default_fitness  # bad fitness
                    fitness_female = base_ff.default_fitness  # bad fitness
                    rmtree(folder_path, ignore_errors=True)
                    fitness = [fitness_male, fitness_female, ind.nodes]
                    return fitness
                else:
                    annual_data_np = Implausibility.read_annual_data(number_years)
                    annual_data_np_list.append(annual_data_np)

            # Delete a model folder
            rmtree(folder_path, ignore_errors=True)

        targets = Implausibility.read_targets('../NormsModel/')
        msv = Implausibility.get_mean_sample_variance('../NormsModel/')
        array = np.array(annual_data_np_list)
        data_mean = np.mean(array, axis=0)
        ip_obj = Implausibility(targets, data_mean, msv, number_years)
        ip_metric = ip_obj.get_ip_metric()

        # create dataframe with data_mean
        annual_data_col_props = ['Prev_male', 'Prev_female', 'Freq_male', 'Freq_female', 'Quant_male', 'Quant_female']
        data_mean_pd = pd.DataFrame(data_mean, columns=annual_data_col_props)

        # create dataframe with fitness and calibration parameters
        fitness_s = pd.Series(ip_obj.get_fitness())
        complexity_s = pd.Series([ind.nodes], index=['complexity'])
        log_odds_s = pd.Series([log_odds_intention], index=['LogOddsIntention'])
        gender_option_s = pd.Series([gender_option], index=['gender_option'])
        params_all = pd.concat([fitness_s, complexity_s, log_odds_s, gender_option_s, calibration_parameters])

        # create dataframe with implausibility for each output along the years
        ip_metric_df = pd.DataFrame(ip_metric, columns=annual_data_col_props)

        # save the data mean derived from annual data, fitnesss, calibration parameter used,
        # and implausibility for each output across time
        save_folder = '../results'
        existing_files_path = os.path.join(save_folder, 'data_mean*')

        plock.acquire()
        # find number of data_mean* files in the folder
        n = len(glob.glob(existing_files_path))
        # save data_mean.csv => equivalent to data mean
        data_mean_pd.to_csv(os.path.join(save_folder, 'data_mean_'+ str(n) + '.csv'), index=False)
        # save fitness_calibration_params.csv
        params_all.to_csv(os.path.join(save_folder, 'fitness_calibration_params_'+ str(n) + '.csv'))
        # save ip_metric.csv
        ip_metric_df.to_csv(os.path.join(save_folder, 'ip_metric_'+ str(n) + '.csv'), index=False)
        process.wait()
        plock.release()

        # fitness for MOGGP
        fitness_male = ip_obj.get_fitness_male()
        fitness_female = ip_obj.get_fitness_female()

        # Return 3 fitness
        fitness = [fitness_male, fitness_female, ind.nodes]
        return fitness

    @staticmethod
    def value(fitness_vector, objective_index):
        """
        This is a static method required by NSGA-II for sorting populations
        based on a given fitness function, or for returning a given index of a
        population based on a given fitness function.

        :param fitness_vector: A vector/list of fitnesses.
        :param objective_index: The index of the desired fitness.
        :return: The fitness at the objective index of the fitness vecror.
        """

        if not isinstance(fitness_vector, list):
            return float("inf")

        return fitness_vector[objective_index]


def replace_norms_code(phenotype, folder_path):
    # replace follows this order because there are overlaps in variable names
    dict = OrderedDict([
        ('Autonomy', 'mAutonomy'),
        ('0.333', '(1.0/3.0)')
    ])

    num_dict = OrderedDict([
        ('Desire', 'mDesire[i]'),
        ('Descriptive', 'mDescriptiveNorm[i]'),
        ('Injunctive', 'mInjunctiveNorm[i]')
    ])

    dem_dict = OrderedDict([
        ('Desire', 'mDesire[0]'),
        ('Descriptive', 'mDescriptiveNorm[0]'),
        ('Injunctive', 'mInjunctiveNorm[0]')
    ])

    strs = phenotype

    # Replaces segments of string x according to the entries of the given dictionary (cdic)
    def numdem_proc(x, cdic):
        for bnf_text, c_variable in cdic.items():
            x = x.replace(bnf_text, c_variable)
        return x

    # Operates on the expression inside safeOdds function in two steps as follows.
    # It is assumed that the expression to operate on, lies between delimiters 'safeOdds(' and ')sddOefas'.
    # 1) duplicates the expression in order to have a numerator and denominator
    # 2) for each duplicated expression use function numdem_proc to replace primitives
    strs = re.sub("(?<=safeOdds\().*?(?=\)sddOefas)",
                  lambda x: numdem_proc(x.group(0), num_dict) + "," + numdem_proc(x.group(0), dem_dict), strs)
    strs = re.sub("sddOefas", "", strs)  # remove string sddOefas from the LogOddsIntention expression

    # Replaces remaining primitives
    for bnf_text, c_variable in dict.items():
        strs = strs.replace(bnf_text, c_variable)
    strs = strs.split(';')
    strs = [ss+';' for ss in strs]  # add the ';' back
    strs.pop()  # remove last empty string
    str_log_odds_intention = strs[0].replace('LogOddsIntention=', 'double temp = ')+'\n'

    template_file = folder_path + '/core/src/TheoryMediator.template.cpp'
    new_file = folder_path + '/core/src/TheoryMediator.cpp'
    with open(template_file) as f:
        new_source = f.read()
        new_source = new_source.replace('//GE-INTENTION', str_log_odds_intention)
    with open(new_file, 'w') as f:
        f.write(new_source)

    return str_log_odds_intention


def get_calibration_parameters(folder_path, gender_option):
    filename_best_params = folder_path + '/lhsSamples_selected.csv'
    best_params = pd.read_csv(filename_best_params)
    best_params = best_params[['Description', 'All', 'Male', 'Female']]
    best_params = best_params.rename(columns={'All': 'all', 'Male': 'male', 'Female': 'female'})
    best_params.set_index('Description', inplace=True)
    best_params = best_params.T
    best_params_select = best_params.loc[gender_option]
    return best_params_select


def automaticity(data, alpha_low, beta_low, alpha_med, beta_med, alpha_high, beta_high, rng):
    # conditions
    cond_low_male = (data['microsim.init.sex'] == 1) & (data['microsim.init.alc.gpd'] < 20)
    cond_med_male = (data['microsim.init.sex'] == 1) & ((data['microsim.init.alc.gpd'] >= 20) & (data['microsim.init.alc.gpd'] <= 100))
    cond_high_male = (data['microsim.init.sex'] == 1) & (data['microsim.init.alc.gpd'] > 100)
    cond_low_female = (data['microsim.init.sex'] == 0) & (data['microsim.init.alc.gpd'] < 20)
    cond_med_female = (data['microsim.init.sex'] == 0) & ((data['microsim.init.alc.gpd'] >= 20) & (data['microsim.init.alc.gpd'] <= 60))
    cond_high_female = (data['microsim.init.sex'] == 0) & (data['microsim.init.alc.gpd'] > 60)

    num_low_male = len(data.loc[cond_low_male])
    num_med_male = len(data.loc[cond_med_male])
    num_high_male = len(data.loc[cond_high_male])
    num_low_female = len(data.loc[cond_low_female])
    num_med_female = len(data.loc[cond_med_female])
    num_high_female = len(data.loc[cond_high_female])

    scipy_random_gen = beta
    scipy_random_gen.random_state = rng
    bvec_low_male = 0.1 + 0.8 * scipy_random_gen.rvs(a=alpha_low, b=beta_low, size=num_low_male)
    bvec_med_male = 0.1 + 0.8 * scipy_random_gen.rvs(a=alpha_med, b=beta_med, size=num_med_male)
    bvec_high_male = 0.1 + 0.8 * scipy_random_gen.rvs(a=alpha_high, b=beta_high, size=num_high_male)

    bvec_low_female = 0.1 + 0.8 * scipy_random_gen.rvs(a=alpha_low, b=beta_low, size=num_low_female)
    bvec_med_female = 0.1 + 0.8 * scipy_random_gen.rvs(a=alpha_med, b=beta_med, size=num_med_female)
    bvec_high_female = 0.1 + 0.8 * scipy_random_gen.rvs(a=alpha_high, b=beta_high, size=num_high_female)

    data.loc[cond_low_male, 'trait.impulsivity'] = bvec_low_male
    data.loc[cond_med_male, 'trait.impulsivity'] = bvec_med_male
    data.loc[cond_high_male, 'trait.impulsivity'] = bvec_high_male
    data.loc[cond_low_female, 'trait.impulsivity'] = bvec_low_female
    data.loc[cond_med_female, 'trait.impulsivity'] = bvec_med_female
    data.loc[cond_high_female, 'trait.impulsivity'] = bvec_high_female

    return {'bvec_low_male': bvec_low_male, 'bvec_med_male': bvec_med_male, 'bvec_high_male': bvec_high_male,
            'bvec_low_female': bvec_low_female, 'bvec_med_female': bvec_med_female, 'bvec_high_female': bvec_high_female}


def autonomy(data, alpha_male, beta_male, alpha_female, beta_female, shift_moderate, shift_heavy, shift_abstainers, rng):
    # conditions
    cond_male = data['microsim.init.sex'] == 1
    cond_female = data['microsim.init.sex'] == 0
    cond_low_freq = data['microsim.init.annual.frequency'] < 36
    cond_mh_freq = (data['microsim.init.annual.frequency'] > 336) | (data['microsim.init.drinks.per.month'] > 100)
    cond_abstainer = data['microsim.init.drinkingstatus'] == 0

    # number of elements based on the conditions
    num_male = len(data.loc[cond_male])
    num_female = len(data.loc[cond_female])

    # generate the beta distributions
    scipy_random_gen = beta
    scipy_random_gen.random_state = rng
    bvec_male = scipy_random_gen.rvs(a=alpha_male, b=beta_male, size=num_male)
    bvec_female = scipy_random_gen.rvs(a=alpha_female, b=beta_female, size=num_female)

    # attribute the beta distributions
    data.loc[cond_male, 'norms.autonomy'] = bvec_male
    data.loc[cond_female, 'norms.autonomy'] = bvec_female

    # apply transformations to the beta distribution values
    moderate_drinkers_shift = shift_moderate * data.loc[cond_low_freq, 'norms.autonomy'] - shift_moderate
    moderate_drinkers_shift = np.exp(moderate_drinkers_shift)
    data.loc[cond_low_freq, 'norms.autonomy'] = moderate_drinkers_shift

    # heavy drinker
    heavy_drinker_shift = shift_heavy * data.loc[cond_mh_freq, 'norms.autonomy'] - shift_heavy
    heavy_drinker_shift = np.exp(heavy_drinker_shift)
    data.loc[cond_mh_freq, 'norms.autonomy'] = heavy_drinker_shift

    # abstainer
    abstainers_shift = shift_abstainers * data.loc[cond_abstainer, 'norms.autonomy'] - shift_abstainers
    abstainers_shift = np.exp(abstainers_shift)
    data.loc[cond_abstainer, 'norms.autonomy'] = abstainers_shift

    return {'bvec_male': bvec_male, 'bvec_female': bvec_female}


def habit_interval(data, loc, scale, rng):
    # parameters
    myclip_a = 18  # clip value in the sample space to the left from loc
    myclip_b = 254  # clip value in the sample space to the right from loc
    a, b = (myclip_a - loc) / scale, (myclip_b - loc) / scale

    n = len(data.index)
    scipy_random_gen = truncnorm
    scipy_random_gen.random_state = rng
    tnorm = scipy_random_gen.rvs(loc=loc, scale=scale, a=a, b=b, size=n)
    data['habit.update.interval'] = tnorm
    return tnorm


def update_agents(folder_path, params_select, rng):

    filename1 = folder_path + '/norms/props/agents.rank1.csv'
    filename2 = folder_path + '/norms/props/spawn.agents.rank1.csv'

    data1 = pd.read_csv(filename1, encoding='ISO-8859-1')
    data2 = pd.read_csv(filename2, encoding='ISO-8859-1')

    # Update Automaticity
    alpha_low = params_select['trait.impulsivity.low.alpha']
    beta_low = params_select['trait.impulsivity.low.beta']
    alpha_med = params_select['trait.impulsivity.med.alpha']
    beta_med = params_select['trait.impulsivity.med.beta']
    alpha_high = params_select['trait.impulsivity.high.alpha']
    beta_high = params_select['trait.impulsivity.high.beta']

    outputs1 = automaticity(data1, alpha_low, beta_low, alpha_med, beta_med, alpha_high, beta_high, rng)
    outputs2 = automaticity(data2, alpha_low, beta_low, alpha_med, beta_med, alpha_high, beta_high, rng)

    bv_low_male_1 = outputs1['bvec_low_male']
    bv_med_male_1 = outputs1['bvec_med_male']
    bv_high_male_1 = outputs1['bvec_high_male']
    bv_low_female_1 = outputs1['bvec_low_female']
    bv_med_female_1 = outputs1['bvec_med_female']
    bv_high_female_1 = outputs1['bvec_high_female']

    bv_low_male_2 = outputs2['bvec_low_male']
    bv_med_male_2 = outputs2['bvec_med_male']
    bv_high_male_2 = outputs2['bvec_high_male']
    bv_low_female_2 = outputs2['bvec_low_female']
    bv_med_female_2 = outputs2['bvec_med_female']
    bv_high_female_2 = outputs2['bvec_high_female']

    # Update Autonomy
    alpha_male = params_select['male.autonomy.alpha']
    beta_male = params_select['male.autonomy.beta']
    alpha_female = params_select['female.autonomy.alpha']
    beta_female = params_select['female.autonomy.beta']
    shift_moderate = 10 ** params_select['shift.autonomy.moderate']
    shift_heavy = 10 ** params_select['shift.autonomy.heavy']
    shift_abstainers = 10 ** params_select['shift.autonomy.abstainers']
    outputs1 = autonomy(data1, alpha_male, beta_male, alpha_female, beta_female,
                        shift_moderate, shift_heavy, shift_abstainers, rng)
    outputs2 = autonomy(data2, alpha_male, beta_male, alpha_female, beta_female,
                        shift_moderate, shift_heavy, shift_abstainers, rng)

    bvec_male_1 = outputs1['bvec_male']
    bvec_female_1 = outputs1['bvec_female']
    bvec_male_2 = outputs2['bvec_male']
    bvec_female_2 = outputs2['bvec_female']

    # update individual habit update interval
    loc = params_select['habit.update.interval.mean']  # mean
    scale = params_select['habit.update.interval.std']  # standard deviation

    tnorm1 = habit_interval(data1, loc, scale, rng)
    tnorm2 = habit_interval(data2, loc, scale, rng)

    data1.to_csv(filename1, index=False)
    data2.to_csv(filename2, index=False)

    d = {'automaticity_beta_low_male_1': bv_low_male_1, 'automaticity_beta_med_male_1': bv_med_male_1,
         'automaticity_beta_high_male_1': bv_high_male_1, 'automaticity_beta_low_female_1': bv_low_female_1,
         'automaticity_beta_med_female_1': bv_med_female_1, 'automaticity_beta_high_female_1': bv_high_female_1,
         'automaticity_beta_low_male_2': bv_low_male_2, 'automaticity_beta_med_male_2': bv_med_male_2,
         'automaticity_beta_high_male_2': bv_high_male_2, 'automaticity_beta_low_female_2': bv_low_female_2,
         'automaticity_beta_med_female_2': bv_med_female_2, 'automaticity_beta_high_female_2': bv_high_female_2,
         'bvec_male_1': bvec_male_1, 'bvec_female_1': bvec_female_1,
         'bvec_male_2': bvec_male_2, 'bvec_female_2': bvec_female_2,
         'tnorm1': tnorm1, 'tnorm2': tnorm2}

    return pd.DataFrame.from_dict(d, orient='index')


def update_model_prop_parameters(folder_path, best_params_select, replication_index=1):

    # get parameters for model.props
    injuctive_threshold = best_params_select['injunctive.threshold']
    injunctive_proportion = best_params_select['injunctive.proportion']
    injunctive_relaxation_prev_adjustment = best_params_select['norms.injunctive.relaxation.prev.adjustment']
    injunctive_punishment_prev_adjustment = best_params_select['norms.injunctive.punishment.prev.adjustment']
    transformational_interval_punish = best_params_select['transformational.interval.punish']
    transformational_interval_relax = best_params_select['transformational.interval.relax']
    transformational_interval_descriptive = best_params_select['transformational.interval.descriptive.norm']
    situational_interval = best_params_select['situational.interval']
    norms_com_days_punish = best_params_select['norms.com.days.punish']
    norms_com_days_relax = best_params_select['norms.com.days.relax']
    bias_factor = best_params_select['bias.factor']
    beta_attitude = best_params_select['beta.attitude']
    beta_norm = best_params_select['beta.norm']
    number_ticks_years = number_years * 365

    # read model.props file
    model_props_file = folder_path + '/norms/props/model.props'
    with open(model_props_file) as f:
        props = f.read()

    props = props.replace('random.seed=93751', 'random.seed=9375' + str(replication_index + 1))
    # implausibility is determined with respect to number_years=26 years only
    props = props.replace('stop.at=11315', 'stop.at=' + str(number_ticks_years))

    props = props.replace('norms.injunctive.threshold=14', 'norms.injunctive.threshold=' + str(injuctive_threshold))
    props = props.replace('norms.injunctive.proportion=0.49',
                          'norms.injunctive.proportion=' + str(injunctive_proportion))
    props = props.replace('norms.injunctive.relaxation.prev.adjustment=1.0',
                          'norms.injunctive.relaxation.prev.adjustment=' + str(injunctive_relaxation_prev_adjustment))
    props = props.replace('norms.injunctive.punishment.prev.adjustment=0.817',
                          'norms.injunctive.punishment.prev.adjustment=' + str(injunctive_punishment_prev_adjustment))
    props = props.replace('transformational.interval.punish=30',
                          'transformational.interval.punish=' + str(transformational_interval_punish))
    props = props.replace('transformational.interval.relax=90',
                          'transformational.interval.relax=' + str(transformational_interval_relax))
    props = props.replace('transformational.interval.descriptive.norm=30',
                          'transformational.interval.descriptive.norm=' + str(transformational_interval_descriptive))
    props = props.replace('situational.interval=7', 'situational.interval=' + str(situational_interval))
    props = props.replace('norms.com.days.punish=30', 'norms.com.days.punish=' + str(norms_com_days_punish))
    props = props.replace('norms.com.days.relax=365', 'norms.com.days.relax=' + str(norms_com_days_relax))
    props = props.replace('bias.factor=0.541', 'bias.factor=' + str(bias_factor))
    props = props.replace('beta.attitude=0.9', 'beta.attitude=' + str(beta_attitude))
    props = props.replace('beta.norm=0.988', 'beta.norm=' + str(beta_norm))

    with open(model_props_file, 'w') as f:
        f.write(props)


class cd:
    """Context manager for changing the current working directory"""
    def __init__(self, newPath):
        self.newPath = os.path.expanduser(newPath)

    def __enter__(self):
        self.savedPath = os.getcwd()
        os.chdir(self.newPath)

    def __exit__(self, etype, value, traceback):
        os.chdir(self.savedPath)