import os # for cd class, path exist
from distutils.dir_util import mkpath, copy_tree, remove_tree # create, copy, delete temp Model folder
import subprocess # to execute shell script
from collections import OrderedDict

from src.fitness.base_ff_classes.base_ff import base_ff

class cd:
    """Context manager for changing the current working directory"""
    def __init__(self, newPath):
        self.newPath = os.path.expanduser(newPath)

    def __enter__(self):
        self.savedPath = os.getcwd()
        os.chdir(self.newPath)

    def __exit__(self, etype, value, traceback):
        os.chdir(self.savedPath)

def replace_norms_code(phenotype, folder_path, replication_index):
    # replace follows this order because there are overlaps in variable names
    dict = OrderedDict([
        ('Autonomy', 'mAutonomy'), 
        ('LogOddsDesire', 'logOddsDesire'),
        ('LogOddsInjNorm', 'logOddsInjNorm'),
        ('LogOddsDesNorm', 'logOddsDesNorm'),
        ('0.333','(1.0/3.0)')
    ])
    strs = phenotype
    for bnf_text, c_variable in dict.items():
        strs = strs.replace(bnf_text, c_variable)
    strs = strs.split(' ')
    str_log_odds_attitude = strs[0].replace('LogOddsAttitude=','return ')+'\n'
    str_log_odds_norms = strs[1].replace('LogOddsNorm=','return ')+'\n'
    str_beta_attitude = strs[2]
    str_beta_norm = strs[3]
    
    str_automaticity_low = strs[4][strs[4].find('=')+1 : -1]
    str_automaticity_med = strs[5][strs[5].find('=')+1 : -1]
    str_automaticity_high = strs[6][strs[6].find('=')+1 : -1]
    
    str_autonomy_male_low = strs[7][strs[7].find('=')+1 : -1]
    str_autonomy_male_med = strs[8][strs[8].find('=')+1 : -1]
    str_autonomy_male_high = strs[9][strs[9].find('=')+1 : -1]
    
    str_autonomy_female_low = strs[10][strs[10].find('=')+1 : -1]
    str_autonomy_female_med = strs[11][strs[11].find('=')+1 : -1]
    str_autonomy_female_high = strs[12][strs[12].find('=')+1 : -1]

    # use R script to update automaticity of agents
    with cd(folder_path):
        cmd_automaticity = 'Rscript updateAutomaticity.R ' + str_automaticity_low + ' ' + str_automaticity_med + ' ' + str_automaticity_high;
        process = subprocess.Popen(cmd_automaticity, shell=True, stdout=subprocess.PIPE)
        process.wait()
        
        cmd_automaticity = 'Rscript updateAutonomy.R ' + str_autonomy_male_low + ' ' + str_autonomy_male_med + ' ' + str_autonomy_male_high + ' ' + str_autonomy_female_low + ' ' + str_autonomy_female_med + ' ' + str_autonomy_female_high;
        process = subprocess.Popen(cmd_automaticity, shell=True, stdout=subprocess.PIPE)
        process.wait()

    # replace parameters in model.props
    model_props_file = folder_path + '/norms/props/model.props'
    with open(model_props_file) as f:
        props = f.read()
        props.replace('random.seed=93751','random.seed=9375'+str(replication_index+1))
        props.replace('stop.at=10950','stop.at=7300') #GE works with 20 years only
        props = props.replace('beta.attitude=0.759',str_beta_attitude[:str_beta_attitude.find(';')])
        props = props.replace('beta.norm=0.842',str_beta_norm[:str_beta_norm.find(';')])
    with open(model_props_file, 'w') as f:
        f.write(props)

    # check theory-related conditions for each term, return false if failed
    if ('logOddsDesire' not in str_log_odds_attitude) or (('logOddsInjNorm' not in str_log_odds_norms) and ('logOddsDesNorm' not in str_log_odds_norms)):
        return False
    else:
        # insert lines of codes to the simulation code
        template_file = folder_path + '/norms/src/NormTheory.template.cpp'
        new_file = folder_path + '/norms/src/NormTheory.cpp'
        with open(template_file) as f:
            new_source = f.read()
            new_source = new_source.replace('//GE-ATTITUDE',str_log_odds_attitude)
            new_source = new_source.replace('//GE-NORM',str_log_odds_norms)
        with open(new_file, 'w') as f:
            f.write(new_source)
        return True

# human structure
#phenotype = 'LogOddsAttitude=(Autonomy*LogOddsDesire); LogOddsNorm=((1-Autonomy)*((LogOddsInjNorm+LogOddsDesNorm)*0.5)); beta.attitude=0.76; beta.norm=0.84;'

#id160 simplest
#phenotype = 'LogOddsAttitude=LogOddsDesire; LogOddsNorm=LogOddsInjNorm; beta.attitude=0.84; beta.norm=0.06;'

#id25 most complex
#phenotype='LogOddsAttitude=(Autonomy*LogOddsDesire); LogOddsNorm=((1-Autonomy)*((((((0.2-((((LogOddsDesNorm-LogOddsDesNorm)-((LogOddsDesNorm+LogOddsDesNorm)*0.3))*(0.01-(LogOddsInjNorm-LogOddsDesNorm)))-0.3))+((((((1-Autonomy)*LogOddsDesNorm)-0.4)*((LogOddsDesNorm-0)*((1-Autonomy)+LogOddsDesNorm)))+4)+0.4))+((0.4-((((LogOddsInjNorm+(LogOddsDesNorm*LogOddsDesNorm))*((LogOddsInjNorm+(1-Autonomy))*(LogOddsInjNorm*(1-Autonomy))))*0.01)*LogOddsDesNorm))-(((9-(0.01-((LogOddsDesNorm*(1-Autonomy))-(1-Autonomy))))-(((LogOddsDesNorm-LogOddsInjNorm)*0.5)*0))-0.2)))-5)+LogOddsDesNorm)*0.5)); beta.attitude=0.74; beta.norm=0.84;'

#id89 size<50, best avg M+F
#phenotype = 'LogOddsAttitude=(((((Autonomy*0.5)*10)-0.5)-0.8)*LogOddsDesire); LogOddsNorm=((1-Autonomy)*(LogOddsInjNorm*0.5)); beta.attitude=1.00; beta.norm=0.87;'

#id27 size<50, best M
#phenotype = 'LogOddsAttitude=(LogOddsDesire*Autonomy); LogOddsNorm=((1-Autonomy)*(LogOddsInjNorm*0.7)); beta.attitude=0.71; beta.norm=0.79;'

#id196 size<50, best F
#phenotype = 'LogOddsAttitude=(Autonomy*LogOddsDesire); LogOddsNorm=((1-Autonomy)*((LogOddsDesNorm*(1-LogOddsInjNorm))*0.5)); beta.attitude=0.87; beta.norm=1.00;'

###old###
#id140 good Male+Female fitness
#phenotype = 'LogOddsAttitude=LogOddsDesire; LogOddsNorm=((1-Autonomy)*((LogOddsInjNorm+LogOddsDesNorm)*0.5)); beta.attitude=0.70; beta.norm=0.66;'

#id159 good size
#phenotype = '	LogOddsAttitude=LogOddsDesire; LogOddsNorm=LogOddsInjNorm; beta.attitude=0.83; beta.norm=0.14;'

#id69 overfit best M+F fitness, max size
#phenotype = 'LogOddsAttitude=(0.9-((0.01-(((((((10-(0.1-log(Autonomy)))*1)+((((LogOddsDesire*0.6)+1)-((LogOddsDesire+0.8)-(log(Autonomy)-Autonomy)))*(((log(Autonomy)+4)-0.7)*(log(Autonomy)*(0.1-LogOddsDesire)))))+((8-(log(Autonomy)*((log(Autonomy)+Autonomy)*Autonomy)))-((((LogOddsDesire+log(Autonomy))+(log(Autonomy)*log(Autonomy)))*0.3)-log(Autonomy))))+0.1)+(0.6-(0.01-(LogOddsDesire-log(Autonomy)))))-log(Autonomy)))-0.9)); LogOddsNorm=((1-Autonomy)*(((((0.8-((0.1-((((LogOddsInjNorm*0.2)+0.4)-((log(1-Autonomy)*(1-Autonomy))-(LogOddsInjNorm-1)))*0.4))+((((((1-Autonomy)+log(1-Autonomy))*((1-Autonomy)*LogOddsDesNorm))*(10-(8-LogOddsDesNorm)))-9)*9)))+(LogOddsDesNorm*((((((log(1-Autonomy)-5)-0.5)*((LogOddsInjNorm*(1-Autonomy))*(0.5-log(1-Autonomy))))+(LogOddsInjNorm+(((1-Autonomy)*LogOddsInjNorm)*(1-Autonomy))))*0.6)*0.7)))*0.5)+LogOddsDesNorm)*0.5)); beta.attitude=0.74; beta.norm=0.65;'

#id124 better male, good female, good size
#phenotype = 'LogOddsAttitude=LogOddsDesire; LogOddsNorm=((1-Autonomy)*((LogOddsInjNorm+LogOddsInjNorm)*0.5)); beta.attitude=0.76; beta.norm=0.46;'

#id 133, same as 124, different parameters
#phenotype = 'LogOddsAttitude=LogOddsDesire; LogOddsNorm=((1-Autonomy)*(LogOddsInjNorm*0.5)); beta.attitude=0.77; beta.norm=0.84;'

#id 190, good female
#phenotype ='LogOddsAttitude=(Autonomy*LogOddsDesire); LogOddsNorm=((1-Autonomy)*((((0.2-((3-((((((1-Autonomy)*LogOddsDesNorm)-(log(1-Autonomy)+1))*(LogOddsDesNorm+10))+(1-Autonomy))+8))-0.1))+(1-Autonomy))+(log(1-Autonomy)+0.5))*0.5)); beta.attitude=0.74; beta.norm=0.84;'
###old###


### iGSS 2021 ###
######### 21_5_4 ############
# human structure (similar automaticity)
#phenotype = 'LogOddsAttitude=(Autonomy*LogOddsDesire); LogOddsNorm=((1-Autonomy)*((LogOddsInjNorm+LogOddsDesNorm)*0.5)); beta.attitude=0.76; beta.norm=0.84; automaticity.low.drinker=left; automaticity.med.drinker=right; automaticity.high.drinker=left;'

# id 104 - simplest & min all 3
#phenotype = 'LogOddsAttitude=LogOddsDesire; LogOddsNorm=LogOddsInjNorm; beta.attitude=0.98; beta.norm=0.07; automaticity.low.drinker=bimodel; automaticity.med.drinker=right; automaticity.high.drinker=left;'

# id 12 - most complex
#phenotype = 'LogOddsAttitude=((LogOddsDesire+3)-(0.1-(((((Autonomy+9)+(((0.4-(LogOddsDesire*((Autonomy+0.9)-(Autonomy-Autonomy))))-1)-((Autonomy+0.4)+((0-(Autonomy*(LogOddsDesire+LogOddsDesire)))-0.01))))+7)+(((0.01-(((1-((Autonomy-6)+0.1))+((0.1-(LogOddsDesire*3))*((LogOddsDesire-9)-(Autonomy*0.2))))*LogOddsDesire))-(((0.01-(((Autonomy+1)*(LogOddsDesire-0))+3))+9)+0))*0))+((0.2-Autonomy)+(LogOddsDesire+LogOddsDesire))))); LogOddsNorm=((((LogOddsDesNorm*((1-Autonomy)-LogOddsInjNorm))-LogOddsDesNorm)*1)*0.4); beta.attitude=0.23; beta.norm=1.00; automaticity.low.drinker=bimodel; automaticity.med.drinker=bimodel; automaticity.high.drinker=right;'

# id 35 - min M+F
#phenotype = 'LogOddsAttitude=(LogOddsDesire-0.4); LogOddsNorm=(((0.7-(1-Autonomy))-(LogOddsInjNorm*LogOddsDesNorm))+(((1-Autonomy)*0.4)-0.4)); beta.attitude=0.52; beta.norm=0.28; automaticity.low.drinker=left; automaticity.med.drinker=left; automaticity.high.drinker=right;'

# id 31 - min M
#phenotype = 'LogOddsAttitude=(LogOddsDesire-0.4); LogOddsNorm=(((4-(1-Autonomy))-(LogOddsInjNorm*LogOddsDesNorm))+(((1-Autonomy)*0.4)-0.4)); beta.attitude=0.52; beta.norm=0.28; automaticity.low.drinker=right; automaticity.med.drinker=left; automaticity.high.drinker=right;'

# id 128 - min F
#phenotype = 'LogOddsAttitude=(LogOddsDesire*((Autonomy+Autonomy)-LogOddsDesire)); LogOddsNorm=(LogOddsDesNorm-5); beta.attitude=0.30; beta.norm=0.01; automaticity.low.drinker=centre; automaticity.med.drinker=left; automaticity.high.drinker=right;'
######### 21_5_4 ############


######### 21_5_31 ############
# id 60 - simplest & min all 3
#phenotype = 'LogOddsAttitude=LogOddsDesire; LogOddsNorm=LogOddsInjNorm; beta.attitude=1.00; beta.norm=0.08; automaticity.low.drinker=bimodel; automaticity.med.drinker=right; automaticity.high.drinker=left;'

# id 39 - most complex
#phenotype = 'LogOddsAttitude=(0.6-((Autonomy-(LogOddsDesire+Autonomy))-7)); LogOddsNorm=(((((((LogOddsDesNorm*0.8)+LogOddsInjNorm)-0)-(0.6-((LogOddsInjNorm*2)*(0.3-(8-(((1-Autonomy)*(((1-Autonomy)-(((LogOddsDesNorm+(0-((((1-Autonomy)+4)+((LogOddsDesNorm+LogOddsDesNorm)+5))+0)))+(LogOddsInjNorm*((LogOddsInjNorm*0.8)*(9-((((1-Autonomy)-0.4)*9)-(((1-Autonomy)+0.7)-8))))))-5))-5))+0.9))))))*0)+1)-(((0.3-((((1-Autonomy)*(LogOddsInjNorm+0.7))-0.7)*5))-0.01)*0.7)); beta.attitude=0.95; beta.norm=0.07; automaticity.low.drinker=left; automaticity.med.drinker=bimodel; automaticity.high.drinker=centre;'

# id 17 - min M+F & min M
#phenotype = 'LogOddsAttitude=(LogOddsDesire-0.5); LogOddsNorm=(((1-Autonomy)*(LogOddsInjNorm+8))-0.7); beta.attitude=0.88; beta.norm=0.33; automaticity.low.drinker=right; automaticity.med.drinker=right; automaticity.high.drinker=left;'

# id 73 - min F
#phenotype = 'LogOddsAttitude=((LogOddsDesire-((((LogOddsDesire+0.3)*LogOddsDesire)+5)+Autonomy))*0.3); LogOddsNorm=(LogOddsDesNorm*0.3); beta.attitude=1.00; beta.norm=0.08; automaticity.low.drinker=bimodel; automaticity.med.drinker=right; automaticity.high.drinker=right;'
######### 21_5_31 ############


# Hand-crafted id 196 (1st ground)
#phenotype = 'LogOddsAttitude=(Autonomy*LogOddsDesire); LogOddsNorm=((1-Autonomy)*((LogOddsDesNorm*(1-LogOddsInjNorm))*0.5)); beta.attitude=0.87; beta.norm=1.00; automaticity.low.drinker=left; automaticity.med.drinker=right; automaticity.high.drinker=left;'

# id125 - size 54 - min F
#phenotype = 'LogOddsAttitude=(LogOddsDesire*((Autonomy+Autonomy)-LogOddsDesire)); LogOddsNorm=((LogOddsInjNorm*0.1)-(((1-Autonomy)*(1-Autonomy))+((1-Autonomy)+10))); beta.attitude=0.30; beta.norm=0.15; automaticity.low.drinker=centre; automaticity.med.drinker=left; automaticity.high.drinker=left;'

if __name__ == "__main__":

    phenotype = 'LogOddsAttitude=LogOddsDesire; LogOddsNorm=LogOddsInjNorm; beta.attitude=0.79; beta.norm=0.12; automaticity.low.drinker=skewed_right; automaticity.med.drinker=skewed_left; automaticity.high.drinker=centre; autonomy.male.abstainer=bi_modal; autonomy.male.moderate=skewed_left; autonomy.male.heavy=skewed_right; autonomy.female.abstainer=bi_modal; autonomy.female.moderate=centre; autonomy.female.heavy=skewed_right;'


    folder_path = '/home/jduro/cascade/ge_norm_igss/TempModels/HumanStructure'

    # Create a model folder
    mkpath(folder_path)

    # Copy simulation into a model folder
    copy_tree('/home/jduro/cascade/ge_norm_igss/NormsModel/', folder_path)

    # Edit norms theory with phenotype
    conditions_flag = replace_norms_code(phenotype, folder_path, 1)

    if not conditions_flag:
        fitness_Male = base_ff.default_fitness # bad fitness
        fitness_Female = base_ff.default_fitness # bad fitness
    else:
        # use cd class (for context manager) to execute the simulation
        # outside the context manager we are back wherever we started.
        with cd(folder_path):
            # Run a shell script: run RepastHPC simulation, and R script for fitness, wait
            process = subprocess.Popen('bash compileThenRun_plot.sh', shell=True, stdout=subprocess.PIPE)
            process.wait()
        
            # Read fitness from file
            if process.returncode != 0: #if error
                fitness_Male = base_ff.default_fitness # bad fitness
                fitness_Female = base_ff.default_fitness # bad fitness
            else:
                # Read fitness from file
                f = open("fitness.out", "r")
                fitness_Male = float(f.readline())
                fitness_Female = float(f.readline())

    print('=================')
    print(phenotype.replace('; ',';\n'))
    print('=> Fitness: ', "{:.3f}".format(fitness_Male), ' ', "{:.3f}".format(fitness_Female))
    print('=================')
