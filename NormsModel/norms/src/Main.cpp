#include <boost/mpi.hpp>
#include "repast_hpc/RepastProcess.h"
#include "repast_hpc/initialize_random.h"

#include "Model.h"
#include "NormModel.h"

void usage(int argc, char** argv) {
  std::cerr << "usage: " << argv[0] << " <config.props> <model.props>" << std::endl;
}

int main(int argc, char** argv){
	std::string configFile = argv[1];
	std::string propsFile = argv[2];

	// {
	// 	int i=0;
	// 	while(0 == i)
	// 		sleep(5);
	// }

	boost::mpi::environment env(argc, argv);
	boost::mpi::communicator world;

	//Read random seed from model.props. If there is no random seed in props file, proceed as normal
	repast::Properties props = repast::Properties(propsFile, argc, argv, &world);
	repast::initializeRandom(props, &world);

	//Set up model
	repast::RepastProcess::init(configFile);

	try {
		/***** FOR TESTING: THROW RANDOM ERRORS *****/
		/*
		if (repast::Random::instance()->nextDouble() < 0.3)
			throw MPI::Exception(MPI::ERR_DIMS); //code 11
			// A custom exception class or more appropraite std:: exception may be better which has a value and message. @todo
			throw std::logic_error("MPI_ERR_DIMS"); //code 11
		else if (repast::Random::instance()->nextDouble() < 0.6)
			throw MPI::Exception(MPI::ERR_INTERN); //code 16
			// A custom exception class or more appropraite std:: exception may be better
			throw std::runtime_error("MPI_ERR_INTERN"); //code 16
		*/
		/***** FOR TESTING: THROW RANDOM ERRORS *****/

		Model* model = new NormModel(propsFile, argc, argv, &world);
		repast::ScheduleRunner& runner = repast::RepastProcess::instance()->getScheduleRunner();
		
		model->initialize(runner); //manange schedule (core)

		runner.run();
		delete model;
		repast::RepastProcess::instance()->done();
		return 0;
	} catch (std::exception& e) {
		std::cerr << "Error " << e.what() << std::endl;
		repast::RepastProcess::instance()->done();
		MPI_Abort(MPI_COMM_WORLD, MPI_ERR_UNKNOWN); // @todo - there might be a more appropraite error code to propagate?
		return EXIT_FAILURE;
	}
}
