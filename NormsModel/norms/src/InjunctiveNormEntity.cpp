#include "repast_hpc/Utilities.h"
#include "repast_hpc/RepastProcess.h"

#include <vector>

#include "globals.h"
#include "NormGlobals.h"
#include "InjunctiveNormEntity.h"
#include "Agent.h"
#include "RegulatorInjunctiveBingePunishment.h"
#include "RegulatorInjunctiveRelaxation.h"

InjunctiveNormEntity::InjunctiveNormEntity(
		std::vector<Regulator*> regulatorList, std::vector<double> powerList, int intervalPunish, int intervalRelax,
		std::vector<std::string> normDataPrev) :
			StructuralEntity(regulatorList, powerList, 1) { //mTransformationalInterval = 1
	mIntervalPunish = intervalPunish;
	mIntervalRelax = intervalRelax;

	//set a pointer to this entity in the relaxation regulator
	((RegulatorInjunctiveRelaxation*) mpRegulatorList[1])->setInjNormEntity(this);

	
	//init norm arrays
	int size = P_REFERENCE_GROUP->size();
	mpInjunctiveNormsPrev = new std::array<double, NUM_SCHEMA>[size];
	for(int i = 0; i < size; ++i) {
		mpInjunctiveNormsPrev[i] = {};
	}

	///////////
	//put data into array
	if (static_cast<int>(normDataPrev.size()) != NUM_SEX*NUM_SCHEMA){
		std::cerr <<  "Missmatch in inj norm data read in and size intended.";
	}

	//SPECIFIC to the reference group design
	int dataIndex = 0;
	for (int i=0; i<NUM_SEX; ++i) {
		for (int k=0; k<NUM_SCHEMA; ++k) { //loop through the data (split by sex, schema)
			for (int j=0; j<NUM_AGE_GROUPS; ++j) { //value is the same for all age groups
				int referenceGroupId = i*NUM_AGE_GROUPS+j;
				mpInjunctiveNormsPrev[referenceGroupId][k] = repast::strToDouble(normDataPrev[dataIndex]);
			}
			dataIndex++;
		}
	}

	// std::cout << "Read in InjNorm" << std::endl;
	// int referenceGroupId = 0;
	// for (int i=0; i<NUM_SEX; ++i) {
	// 	for (int j=0; j<NUM_AGE_GROUPS; ++j) { //value is the same for all age groups
	// 		for (int k=0; k<NUM_SCHEMA; ++k) { //loop through the data (split by sex, schema)
	// 			printf("%.2f\t",mpInjunctiveNormsPrev[referenceGroupId][k]);
	// 		}
	// 		printf("\n");
	// 		referenceGroupId++;
	// 	}
	// }
	///////////

}

InjunctiveNormEntity::~InjunctiveNormEntity() {
	delete[] mpInjunctiveNormsPrev;
}

double InjunctiveNormEntity::getInjNormPrev(int groupId, int schemaId) {
	return mpInjunctiveNormsPrev[groupId][schemaId];
}

void InjunctiveNormEntity::doTransformation() {
	int currentTick = (int)floor(repast::RepastProcess::instance()->getScheduleRunner().currentTick());
	if ((currentTick % mTransformationalInterval) == 0) {
		mTransformationalTriggerCount++;

		//call all regulators to update adjustment value
		RegulatorInjunctiveBingePunishment* pRegPunish = (RegulatorInjunctiveBingePunishment*) mpRegulatorList[0];
		RegulatorInjunctiveRelaxation* pRegRelax = (RegulatorInjunctiveRelaxation*) mpRegulatorList[1];

		if (currentTick % mIntervalPunish == 0)
			pRegPunish->updateAdjustmentLevel();
		else
			pRegPunish->resetAdjustmentLevel();

		if (currentTick % mIntervalRelax == 0)
			pRegRelax->updateAdjustmentLevel();
		else
			pRegRelax->resetAdjustmentLevel();

		//update injunctive norm based on adjustment level values from regulators
		// bool flag = false;
		int size = P_REFERENCE_GROUP->size();

		//update inj norm with the two mechanisms: punish & relax
		for (int groupId = 0; groupId != size; groupId++) {
			double punishModifier = pRegPunish->getAdjustmentLevelPrev(groupId);
			double relaxModifier  = pRegRelax->getAdjustmentLevelPrev(groupId);

			// if (punishModifier!=1 || relaxModifier!=1) {
			// 	printf("%d - punish %.2f - relax %.2f\n", groupId, punishModifier, relaxModifier);
			// 	flag = true;
			// }
			
			//punish is specific to the reference group design
			//PUNISH: the highest schema 5
			mpInjunctiveNormsPrev[groupId][NUM_SCHEMA-1] *= punishModifier;

			//PUNISH: if male => also punish schema 4
			if (groupId / NUM_AGE_GROUPS == MALE)
				mpInjunctiveNormsPrev[groupId][NUM_SCHEMA-2] *= punishModifier;
			
			//RELAX: all drinking schema 2-5 (exclude abstain schema 1)
			double proportionCurrentDrinkers;
			for (int schemaId=1; schemaId<NUM_SCHEMA; schemaId++) {
				proportionCurrentDrinkers = pRegRelax->getPropotionCurrentDrinkers(groupId, schemaId);
				mpInjunctiveNormsPrev[groupId][schemaId] = 
					relaxModifier * mpInjunctiveNormsPrev[groupId][schemaId] + 
					(1-relaxModifier) * proportionCurrentDrinkers;
			}
		}

		// if (flag) {
		// 	std::cout << "New Inj Norm - temp values" << std::endl;
		// 	int referenceGroupId = 0;
		// 	for (int i=0; i<NUM_SEX; ++i) {
		// 		for (int j=0; j<NUM_AGE_GROUPS; ++j) { //value is the same for all age groups
		// 			printf("%d\t", referenceGroupId);
		// 			for (int k=0; k<NUM_SCHEMA; ++k) { //loop through the data (split by sex, schema)
		// 				printf("%.4f ", mpInjunctiveNormsPrev[referenceGroupId][k]);
		// 			}
		// 			printf("\n");
		// 			referenceGroupId++;
		// 		}
		// 	}
		// }

		//normalization
		for (int groupId = 0; groupId != size; groupId++) {
			double totalPrev = 0;
			for (int schemaId=0; schemaId<NUM_SCHEMA; schemaId++)
				totalPrev += mpInjunctiveNormsPrev[groupId][schemaId];
			for (int schemaId=0; schemaId<NUM_SCHEMA; schemaId++)
				mpInjunctiveNormsPrev[groupId][schemaId] /= totalPrev;
		}

		// if (flag) {
		// 	std::cout << "New Inj Norm - normalization" << std::endl;
		// 	int referenceGroupId = 0;
		// 	for (int i=0; i<NUM_SEX; ++i) {
		// 		for (int j=0; j<NUM_AGE_GROUPS; ++j) { //value is the same for all age groups
		// 			printf("%d\t", referenceGroupId);
		// 			for (int k=0; k<NUM_SCHEMA; ++k) { //loop through the data (split by sex, schema)
		// 				printf("%.4f ", mpInjunctiveNormsPrev[referenceGroupId][k]);
		// 			}
		// 			printf("\n");
		// 			referenceGroupId++;
		// 		}
		// 	}
		// }

		// EXPERIMENT 3
		/*if(repast::RepastProcess::instance()->getScheduleRunner().currentTick() >= 365*5 && !mInterventionFlag){
			//std::cout << "==B== " << repast::RepastProcess::instance()->getScheduleRunner().currentTick() << std::endl;
			for (int currentSex = 0; currentSex != NUM_SEX; ++currentSex) {
				for (int currentAgeGroup = 0; currentAgeGroup != NUM_AGE_GROUPS; ++currentAgeGroup) {
					//std::cout << currentSex <<"\t" << currentAgeGroup <<"\t" <<mpInjunctiveNormsGate[currentSex][currentAgeGroup];
					mpInjunctiveNormsGate[currentSex][currentAgeGroup] /= 2;
					//std::cout << "\t" << mpInjunctiveNormsGate[currentSex][currentAgeGroup] << std::endl;
				}
			}
			//std::cout << "==E== " << repast::RepastProcess::instance()->getScheduleRunner().currentTick() << std::endl << std::endl;
			mInterventionFlag = true;
		}*/
	}
}
