#include "DescriptiveNormEntity.h"
#include "globals.h"
#include "NormGlobals.h"

#include <vector>
#include "repast_hpc/Utilities.h"
#include "repast_hpc/RepastProcess.h"
#include <math.h>
#include <stdio.h>

DescriptiveNormEntity::DescriptiveNormEntity(
		std::vector<Regulator*> regulatorList, std::vector<double> powerList, int transformationalInterval,
		repast::SharedContext<Agent> *context) :
			StructuralEntity(regulatorList, powerList, transformationalInterval) {
	mpContext = context;

	//init norm arrays
	int size = P_REFERENCE_GROUP->size();
	mAvgPrevalence = new std::array<double, NUM_SCHEMA>[size];
	for(int i = 0; i < size; i++) {
		mAvgPrevalence[i] = {};
	}
}

DescriptiveNormEntity::~DescriptiveNormEntity() {
	delete[] mAvgPrevalence;
}

void DescriptiveNormEntity::updateDescriptiveGroupDrinking() {
	int size = P_REFERENCE_GROUP->size();

	//reset avg arrays
	for (int i=0; i<size; i++)
		for (int j=0; j<NUM_SCHEMA; j++)
			mAvgPrevalence[i][j] = 0;

	//create arrays to count agents in a group
	int* pCountInGroup = new int[size]{};

	//loop through agents to calculate average
	std::vector<Agent*> agents;
	mpContext->selectAgents(mpContext->size(), agents);
	// Sort and shuffle the vector to address reproducibillity issues
	Agent::sortAndShuffleAgentPointers(agents);
	std::vector<Agent*>::const_iterator iter = agents.begin();
	std::vector<Agent*>::const_iterator iterEnd = agents.end();
	while (iter != iterEnd) {
		int sex = (*iter)->getSex();
		int ageGroup = (*iter)->findAgeGroup();
		int groupId = P_REFERENCE_GROUP->getId(sex, ageGroup);

		std::array<int, NUM_SCHEMA> countSchemas = (*iter)->getSchemaCountOverNDays(N_DAYS_DESCRIPTIVE);
		for (int schemaId=0; schemaId<NUM_SCHEMA; schemaId++) {
			mAvgPrevalence[groupId][schemaId] += (double)countSchemas[schemaId] / (double)N_DAYS_DESCRIPTIVE;
		}

		pCountInGroup[groupId]++;
		iter++;
	}

	//caculate aggregate info
	//printf("Tick %.1f - Des norm entity\n", repast::RepastProcess::instance()->getScheduleRunner().currentTick());
	for (int i=0; i<size; i++) {
		for (int j=0; j<NUM_SCHEMA; j++) {
			if (pCountInGroup[i] == 0)
				mAvgPrevalence[i][j] = 0;
			else mAvgPrevalence[i][j] /= pCountInGroup[i];
			//printf("%.2f\t",mAvgPrevalence[i][j]);
		}
		//printf("\n");
	}

	delete[] pCountInGroup;
}

void DescriptiveNormEntity::doTransformation() {
	int currentTick = (int)floor(repast::RepastProcess::instance()->getScheduleRunner().currentTick());
	if ((currentTick % mTransformationalInterval) == 0) {
		mTransformationalTriggerCount++;
		updateDescriptiveGroupDrinking(); //update avg drinking values
	}
}
