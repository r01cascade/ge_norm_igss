#include "RegulatorInjunctiveRelaxation.h"
#include "NormGlobals.h"

RegulatorInjunctiveRelaxation::RegulatorInjunctiveRelaxation(repast::SharedContext<Agent> *context) {
	mpContext = context;

	//init arrays
	int size = P_REFERENCE_GROUP->size();
	mpAdjustmentLevelsPrev = new double[size]{};
	mTransformationalTriggerCount = new int[size]{};
	mpProportionCurrentDrinkers = std::vector<std::array<double, NUM_SCHEMA>>(size);
	
	resetCount();
}

RegulatorInjunctiveRelaxation::~RegulatorInjunctiveRelaxation() {
	delete[] mpAdjustmentLevelsPrev;
	delete[] mTransformationalTriggerCount;
	int size = P_REFERENCE_GROUP->size();
}

void RegulatorInjunctiveRelaxation::resetCount() {
	for (int i=0; i<P_REFERENCE_GROUP->size(); ++i)
		mTransformationalTriggerCount[i] = 0;
}

void RegulatorInjunctiveRelaxation::resetAdjustmentLevel() {
	for (int i=0; i<P_REFERENCE_GROUP->size(); ++i)
		mpAdjustmentLevelsPrev[i] = 1;
}

void RegulatorInjunctiveRelaxation::updateAdjustmentLevel() {
	int size = P_REFERENCE_GROUP->size();

	//init array, reset to 0
	int countN [size] = {0};
	int countCurrentDrinker [size] = {0};
	// Reset mpProportionCurrentDrinkers to 0.
	for (int i = 0; i < size; i++) {
		for (int j = 0; j < NUM_SCHEMA; j++) {
			mpProportionCurrentDrinkers[i][j] = 0.0;
		}
	}
	// for (int i=0; i<size; ++i) {
	// 	countN[i] = 0;
	// 	countCurrentDrinker[i] = 0;
	// }

	//loop through agent and count num of drinkers
	repast::SharedContext<Agent>::const_local_iterator iter = mpContext->localBegin();
	repast::SharedContext<Agent>::const_local_iterator iterEnd = mpContext->localEnd();
	while (iter != iterEnd) {
		int groupId = P_REFERENCE_GROUP->getId((*iter)->getSex(), (*iter)->findAgeGroup());
		++countN[groupId];
		if ((*iter)->isHaveKDrinksOverNDays(COMP_DAYS_RELAX, 1)) {
			++countCurrentDrinker[groupId];
		}

		std::array<int, NUM_SCHEMA> countSchemas = (*iter)->getSchemaCountOverNDays(COMP_DAYS_RELAX);
		for (int schemaId=0; schemaId<NUM_SCHEMA; schemaId++) {
			mpProportionCurrentDrinkers[groupId][schemaId] += (double)countSchemas[schemaId] / (double)COMP_DAYS_RELAX;
		}

		iter++;
	}

	for (int i=0; i<size; ++i) {
		double proportionCurrentDrinkers = 0;
		if (countN[i] != 0){
			proportionCurrentDrinkers = double(countCurrentDrinker[i]) / double(countN[i]);
			for (int schemaId=0; schemaId<NUM_SCHEMA; schemaId++) {
				mpProportionCurrentDrinkers[i][schemaId] /= double(countN[i]);
			}
		} else {
			//std::cout << "There are no people in this demographic here" << std::endl;
		}

		//std::cout << "== descriptive proportion drinkers: " << i << " " << proportionCurrentDrinkers << std::endl;
		double propotionDrinkersInjunctiveNorm = 0;
		for (int j=1; j<NUM_SCHEMA; j++) //only include drinking schema
			propotionDrinkersInjunctiveNorm += mpInjunctiveNormEntity->getInjNormPrev(i,j);
		
		if (proportionCurrentDrinkers > propotionDrinkersInjunctiveNorm) {
			mTransformationalTriggerCount[i]++;
			mpAdjustmentLevelsPrev[i] = INJ_RELAXATION_PREV_ADJUSTMENT;
			//std::cout << "The injunctive norm was adjusted (relax): " << i << " " << j << std::endl;
		} else {
			mpAdjustmentLevelsPrev[i] = 1;
			//std::cout << "The injunctive norm was unchanged: " << i << " " << j << " " << mpInjunctiveNormEntity->getInjNormGate(i,j) << std::endl;
		}
	}
/* #ifdef DEBUG
				std::stringstream msg;
				msg
						<< "Heavy drinkers had to be punished. The new injunctive norm of this group is now: "
						<< mpInjunctiveNorms[currentSex][currentAgeGroup]
						<< "\n";
				std::cout << msg.str();
#endif */


}

void RegulatorInjunctiveRelaxation::setInjNormEntity(InjunctiveNormEntity *pInjNormEntity) {
	mpInjunctiveNormEntity = pInjNormEntity;
}

double RegulatorInjunctiveRelaxation::getAdjustmentLevelPrev(int groupId) {
	return mpAdjustmentLevelsPrev[groupId];
}

double RegulatorInjunctiveRelaxation::getPropotionCurrentDrinkers(int groupId, int schemaId) {
	return mpProportionCurrentDrinkers[groupId][schemaId];
}