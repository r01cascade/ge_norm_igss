#include <math.h>

#include "repast_hpc/RepastProcess.h"

#include "Agent.h"
#include "Theory.h"
#include "NormTheory.h"
#include "NormGlobals.h"

NormTheory::NormTheory(repast::SharedContext<Agent> *pPopulation, InjunctiveNormEntity *pInjNormEntity,
		DescriptiveNormEntity *pDesNormEntity) {
	mAutonomy = double (repast::Random::instance()->nextDouble());

	mpPopulation = pPopulation;
	mpInjNormEntity = pInjNormEntity;
	mpDesNormEntity = pDesNormEntity;

	mDesires = {};
	mDescriptiveNormPrev = {};
}

NormTheory::NormTheory(repast::SharedContext<Agent> *pPopulation, InjunctiveNormEntity *pInjNormEntity,
		DescriptiveNormEntity *pDesNormEntity, double autonomy) {
	mAutonomy = autonomy;

	mpPopulation = pPopulation;
	mpInjNormEntity = pInjNormEntity;
	mpDesNormEntity = pDesNormEntity;
	
	mDesires = {};
	mDescriptiveNormPrev = {};
}

void NormTheory::initDesires() {
	//prob of desire gateway: frequency => random num of days => bump prob down for freq 1
	int freq = mpAgent->getDrinkFrequencyLevel();
	int freqLowerBound[7] = {0 ,11,19,31, 54,172,337};
	int freqUpperBound[7] = {10,18,30,53,171,336,365};
	int dayLowerBound = freqLowerBound[freq-1];
	int dayUpperBound = freqUpperBound[freq-1];
	repast::IntUniformGenerator rnd = repast::Random::instance()->createUniIntGenerator(dayLowerBound, dayUpperBound);
	int randomDrinkingDaysPerYear = rnd.next();
	mDesireGateway = (double)randomDrinkingDaysPerYear/365.0;
	if (freq<3) { //bump down for low freq
		mDesireGateway *= DESIRE_MULTIPLIER_ABSTAINER;
	} 
	if(freq > 6) { //bump up for high freq
		mDesireGateway *= DESIRE_MULTIPLIER_DRINKER;
	}

	//desire mean and sd: init (once) from drinking history
	mDesireMean = mpAgent->getPastYearMeanDrink();
	//std::cout << "This is the desired mean: " << mDesireMean << std::endl;
	if (mDesireMean>1) { //bump up for drinkers
		mDesireMean *= DESIRE_MULTIPLIER_DRINKER;
	}
	mDesireSd = mpAgent->getPastYearSdDrink();

	//NEW ACTION MECH	
	//init desire as from past year used schema
	mDesires = {};
	std::array<int, NUM_SCHEMA> countSchemas = mpAgent->getSchemaCountOverNDays(365);
	for (int schemaId=0; schemaId<NUM_SCHEMA; schemaId++) {
		mDesires[schemaId] += (double)countSchemas[schemaId] / 365.0;
	}
}

NormTheory::~NormTheory(){}

void NormTheory::doSituation() {
	updateDescriptiveNorm();
}

double NormTheory::calcDispositionFunc(double autonomy, double payoff, double injunctive, double descriptive) {
	return (payoff * autonomy) + ( (sqrt(injunctive*descriptive)) * (1 - autonomy));
}

void NormTheory::doGatewayDisposition() {}

void NormTheory::doNextDrinksDisposition() {}

void NormTheory::doNonDrinkingActions() {
	//do nothing
}

void NormTheory::updateDescriptiveNorm() {
	double weights = 0;
	std::array<double, NUM_SCHEMA> weightedDrinking = {};
	int groupId = P_REFERENCE_GROUP->getId(mpAgent->getSex(), mpAgent->findAgeGroup());
	for (int i=0; i<P_REFERENCE_GROUP->size(); i++) {
		int w = P_REFERENCE_GROUP->compare(groupId,i);
		weights += w;
		for (int j=0; j<NUM_SCHEMA; j++) {
			weightedDrinking[j] += w * mpDesNormEntity->getAvgPrevalence(i)[j];
		}
	}

	if (weights != 0) {
		std::array<int, NUM_SCHEMA> countSchemas = mpAgent->getSchemaCountOverNDays(N_DAYS_DESCRIPTIVE);
		for (int i=0; i<NUM_SCHEMA; i++) {
			mDescriptiveNormPrev[i] = weightedDrinking[i]/weights * PERCEPTION_BIAS + 
					(double)countSchemas[i]/(double)N_DAYS_DESCRIPTIVE * (1-PERCEPTION_BIAS);
		}
		// Introduce bias in perceiving the norm
		//std::cout << "descriptive before bias " << mDescriptiveNormQuant  << std::endl;
		// EXPERIMENT 1 
		//if(repast::RepastProcess::instance()->getScheduleRunner().currentTick() >= 365*5 && pastNDaysMeanDrink>mDescriptiveNormQuant){
		//mDescriptiveNormQuant = mDescriptiveNormQuant * 0.95 +  pastNDaysMeanDrink * 0.05;
		//} else {
		//mDescriptiveNormQuant = mDescriptiveNormQuant * PERCEPTION_BIAS +  pastNDaysMeanDrink * (1-PERCEPTION_BIAS);
		//}
		//std::cout << "descriptive after bias " << mDescriptiveNormQuant << std::endl;
	} else {
		std::cerr << "Descriptive Norm: weights should NOT be 0." << std::endl;
	}
}

double NormTheory::getAttitude(DrinkingSchema schema) {
	int schemaId = static_cast<int>(schema);
	double logOddsDesire = safeLog(safeOdds(mDesires[schemaId], mDesires[0]));
	
	//GE-ATTITUDE
	//return mAutonomy*logOddsDesire;
}

double NormTheory::getNorm(DrinkingSchema schema){
	int groupId = P_REFERENCE_GROUP->getId(mpAgent->getSex(), mpAgent->findAgeGroup());
	int schemaId = static_cast<int>(schema);
	
	double injunctive = safeOdds(mpInjNormEntity->getInjNormPrev(groupId, schemaId), mpInjNormEntity->getInjNormPrev(groupId, 0));
	double descriptive = safeOdds(mDescriptiveNormPrev[schemaId], mDescriptiveNormPrev[0]);

	double logOddsInjNorm = safeLog(injunctive);
	double logOddsDesNorm = safeLog(descriptive);
	
	//GE-NORM
	//return (1-mAutonomy)*(logOddsInjNorm+logOddsDesNorm)/2;
}

double NormTheory::getPerceivedBehaviourControl(DrinkingSchema schema) {
	return 0;
}

double NormTheory::getAutonomy() {return mAutonomy;}

double* NormTheory::getDesires() {return mDesires;}

void NormTheory::setDesires(double* desires) {
	for (int i=0; i<NUM_SCHEMA; i++)
		mDesires[i]=desires[i];
}

double NormTheory::weighted_sum(double num1, double num2, double weight) {
	return weight*num1 + (1-weight)*num2;
}
