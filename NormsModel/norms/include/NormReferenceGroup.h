#ifndef INCLUDE_NORM_REFERENCE_GROUP_H_
#define INCLUDE_NORM_REFERENCE_GROUP_H_
#include <tuple>

class NormReferenceGroup {

private:
	int mNumberOfReferenceGroups;
	std::tuple<int, int> *mReferenceGroups;

public:
	NormReferenceGroup();
	~NormReferenceGroup();

	int size();

	int getId(int sex, int ageGroup);
	int compare(int id1, int id2);
};

#endif /* INCLUDE_NORM_REFERENCE_GROUP_H_ */