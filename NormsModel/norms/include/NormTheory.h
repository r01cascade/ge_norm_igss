#ifndef NORM_THEORY_H_
#define NORM_THEORY_H_

#include <array>

#include "Theory.h"
#include "Agent.h"
#include "repast_hpc/SharedContext.h"
#include "InjunctiveNormEntity.h"
#include "DescriptiveNormEntity.h"
#include "NormReferenceGroup.h"

#include <math.h>
#include <boost/mpi.hpp>
#include <boost/lexical_cast.hpp>
#include "repast_hpc/logger.h"

class NormTheory : public Theory {

private:
	double mAutonomy;
	double mDesireGateway;
	double mDesireMean;
	double mDesireSd;
	std::array<double, NUM_SCHEMA> mDesires;
	std::array<double, NUM_SCHEMA> mDescriptiveNormPrev;
	repast::SharedContext<Agent> *mpPopulation;
	InjunctiveNormEntity *mpInjNormEntity;
	DescriptiveNormEntity *mpDesNormEntity;
	double mOneTimeRandomForDisposition;

/*	//This function takes a list of drinkingValues range:(0,1) calculated by calcCurrentDrinking and returns a number of drinks
	//consumed in the drinking session. It also takes a bool isDeterministic so that whatever calls the method
	//can decide wether to use the deterministic version of the method or the probablistic version.
	int doDrinkingEngine(std::vector<double> drinkingValues, bool isDeterministic = true);

	//overloaded version of doDrinkingEngine that takes a single double drinkingValue range (0,1) (produced by the calcCurrentDrinking method)
	//a bool on whether to use the determinisitic or probabilitistic version of the code, and returns a number of drinks consumed: 1 or 0.
	//isDeterministic is defaulted to TRUE, so you can call the function with just the drinkingValue argument.
	int doDrinkingEngine(double drinkingValue, bool isDeterministic = true);
*/
	/* Situational mechanisms */
	void updateDescriptiveNorm();

	int findAgeGroup();
	double calcDispositionFunc(double autonomy, double payoff, double injunctive, double descriptive);

	double weighted_sum(double num1, double num2, double weight);

public:
	NormTheory(repast::SharedContext<Agent> *pPopulation, InjunctiveNormEntity *pInjNormEntity,
			DescriptiveNormEntity *pDesNormEntity);
	NormTheory(repast::SharedContext<Agent> *pPopulation, InjunctiveNormEntity *pInjNormEntity,
			DescriptiveNormEntity *pDesNormEntity, double autonomy);
	~NormTheory();

	void doSituation() override;
	void doGatewayDisposition();
	void doNextDrinksDisposition();
	void doNonDrinkingActions() override;

	void initDesires();

	/* NEW ACTION MECH */
	double getAttitude(DrinkingSchema schema) override;
	double getNorm(DrinkingSchema schema) override;
	double getPerceivedBehaviourControl(DrinkingSchema schema) override;

	double getAutonomy() override;
	std::array<double, NUM_SCHEMA> getDesires();
	void setDesires(std::array<double, NUM_SCHEMA> desires);

	double getDesire(DrinkingSchema schema) override;
	double getDescriptiveNorm(DrinkingSchema schema) override;
	double getInjunctiveNorm(DrinkingSchema schema) override;

	InjunctiveNormEntity* getInjNormEntity() {return mpInjNormEntity;}
	DescriptiveNormEntity* getDesNormEntity() {return mpDesNormEntity;}
};

#endif /* NORM_THEORY_H_ */
