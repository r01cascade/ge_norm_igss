#ifndef INCLUDE_DESCRIPTIVE_NORM_ENTITY_H_
#define INCLUDE_DESCRIPTIVE_NORM_ENTITY_H_

#include <array>
#include "StructuralEntity.h"
#include "repast_hpc/SharedContext.h"
#include "Agent.h"

class DescriptiveNormEntity : public StructuralEntity {

private:
	repast::SharedContext<Agent> *mpContext;
	std::array<double, NUM_SCHEMA>* mAvgPrevalence;

public:
	DescriptiveNormEntity(std::vector<Regulator*> regulatorList, std::vector<double> powerList,
			int transformationalInterval,
			repast::SharedContext<Agent> *context);
	~DescriptiveNormEntity();
	void doTransformation() override;

	void updateDescriptiveGroupDrinking();
	// This function is used very frequently, inlining it in the header should reduce overhead costs associated with it.
	inline std::array<double, NUM_SCHEMA> getAvgPrevalence(const int groupId) {
		return mAvgPrevalence[groupId];
	}
};

#endif /* INCLUDE_DESCRIPTIVE_NORM_ENTITY_H_ */
