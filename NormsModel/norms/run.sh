#!/bin/bash

#The following parameters must be provided when run the script
# $1: number of cores

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
cd $DIR
rm -f outputs/*
mpirun -n $1 ./bin/main.exe ./props/config.props ./props/model.props

