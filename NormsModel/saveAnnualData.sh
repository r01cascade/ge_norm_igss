#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
cd $DIR

# save ./norms/outputs/annual_data.csv
cd ../../results/
Number=`ls annual_data* 2> /dev/null  | wc -l`
cd $DIR
cp ${DIR}/norms/outputs/annual_data.csv ../../results/annual_data_${Number}.csv
cp ${DIR}/norms/outputs/fitness_calibration_params.csv ../../results/fitness_calibration_params_${Number}.csv
cp ${DIR}/norms/outputs/ip_metric.csv ../../results/ip_metric_${Number}.csv
# cp ${DIR}/norms/outputs/beta_distributions.csv ../../results/beta_distributions_${Number}.csv
