#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
cd $DIR
rm -f fitness.out
rm -f norms/outputs/*
cd norms
make
mpirun -n 1 --bind-to none ./bin/main.exe ./props/config.props ./props/model.props
cd $DIR
Rscript calculateFitness_all.R

# save ./norms/outputs/annual_data.csv
cd ../../results/
Number=`ls annual_data* 2> /dev/null  | wc -l`
cd $DIR
cp ${DIR}/norms/outputs/annual_data.csv ../../results/annual_data_${Number}.csv
