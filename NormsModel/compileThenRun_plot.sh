DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
cd $DIR
rm -f fitness.out
rm -f norms/outputs/*
cd norms
make
mpirun -n 1 --bind-to none ./bin/main.exe ./props/config.props ./props/model.props
cd $DIR
Rscript calculateFitness_plot.R
