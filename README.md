# About
This is the source code for the simulation model and the model discovery process (grammatical evolution) by [CASCADE project](https://www.sheffield.ac.uk/cascade) for a publication  in the Journal of Artificial Societies and Social Simulation (JASSS).

Vu, Tuong Manh, Buckley, Charlotte, Duro, João A., Brennan, Alan, Epstein, Joshua M. and Purshouse, Robin C. (2023) 'Can Social Norms Explain Long-Term Trends in Alcohol Use? Insights from Inverse Generative Social Science' Journal of Artificial Societies and Social Simulation 26 (2) 4 <http://jasss.soc.surrey.ac.uk/26/2/4.html>. doi: 10.18564/jasss.5077

This repo is hereby licensed for use under the GNU GPL version 3.

# Folders and files structure
* The NormsModel folder is the source code of the hybrid simulation model. The model needs to be compiled before starting the model discovery process. Please clone the "core" repository into the NormsModel folder (./NormsModel/core/), then use git to switch to the branch JoaoiGSSNewGrammar. Then compile the model (instruction below).
* The model discovery process uses [PonyGE2](https://github.com/PonyGE/PonyGE2), which already includes in this repository. We added files to the original PonyGE2 installation. These three files are used for the model discovery process in the paper. Please prefer to the next section for running the model discovery process.
    * parameters/cascade\_norms\_3fitness\_action\_mech\_new\_gram2.txt: defines the settings of the model discovery process.
    * grammars/cascade\_norms\_action\_mech\_new\_gram2.bnf defines the grammar. Note that the wording used in this implementation is slightly different from the paper, but there is no different in functionality.
    * src/fitness/cascade\_norms\_3fitness\_action\_mech\_new\_gram2.py calculates model errors by comparing the simulated outputs with the real-world target data.

# Compile the norms model
* Install [RepastHPC](https://repast.github.io/repast_hpc.html).
* Update the Model/hybrid/env file to match with the RepastHPC installation.
* In the terminal, change directory to Model/hybrid then execute the command: make all

# Run the model discovery process with PonyGE2
* Make sure the model has compiled successfully.
* Make sure the folder paths are correct (MODELS\_FOLDER, SIMULATION\_FOLDER) in the setting file cascade\_norms\_3fitness\_action\_mech\_new\_gram2.txt
* Run the model discovery process by navigating to ./src folder then typing this command: python3 ponyge.py --parameters cascade\_norms\_3fitness\_action\_mech\_new\_gram2.txt
