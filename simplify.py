from sympy import *
x, y, z, t = symbols('x y z t')
k, m, n = symbols('k m n', integer=True)
f, g, h = symbols('f g h', cls=Function)

# Source: https://stackoverflow.com/questions/48491577/printing-the-output-rounded-to-3-decimals-in-sympy
def round_expr(expr, num_digits):
    return expr.xreplace({n : round(n, num_digits) for n in expr.atoms(Number)})

#gp121
#phenotype = 'RoleSelectionMech=OFF; RoleSocialisationMech=ON; RoleLoad=((InvolvedxMarital-EmploymentStatus)+(60.9521*((1-ParenthoodStatus)*InvolvedxParenthood))); RoleIncongruence=(((0.12825+((0.333*DiffExpectancyMarital)+(0.01*DiffExpectancyParenthood)))+(1-(0.333*DiffExpectancyParenthood)))+(0.333*DiffExpectancyEmployment)); RoleStrain=((0.5*RoleIncongruence)+(2.0106*RoleIncongruence)); OutMod=((1-(0.25357*RoleLoad))+(0.001*EmploymentStatus)); InMod=((1-(23.78638*RoleLoad))+((1-(23.78638*RoleLoad))+(2*RoleLoad))); probFirstDrink=((LifetimeDispositionI*LifetimeDispositionI)*(1+(1.10418*RoleStrain))); probNextDrink=LifetimeDispositionI;'

#gp79
phenotype = 'LogOddsAttitude=(LogOddsDesire-0.4); LogOddsNorm=(((0.7-(1-Autonomy))-(LogOddsInjNorm*LogOddsDesNorm))+(((1-Autonomy)*0.4)-0.4)); beta.attitude=0.52; beta.norm=0.28; automaticity.low.drinker=left; automaticity.med.drinker=left; automaticity.high.drinker=right;'

ON, OFF, MaritalStatus, ParenthoodStatus, EmploymentStatus, InvolvedxMarital, InvolvedxParenthood, InvolvedxEmployment, DiffExpectancyMarital, DiffExpectancyParenthood, DiffExpectancyEmployment, RoleLoad, RoleIncongruence, RoleStrain, ProbOppIn, ProbOppOut, LifetimeDispositionI = symbols('ON OFF MaritalStatus ParenthoodStatus EmploymentStatus InvolvedxMarital InvolvedxParenthood InvolvedxEmployment DiffExpectancyMarital DiffExpectancyParenthood DiffExpectancyEmployment RoleLoad RoleIncongruence RoleStrain ProbOppIn ProbOppOut LifetimeDispositionI')
eqs=phenotype.split(' ')
for eq in eqs:
	id_start = eq.find('=')
	id_end = eq.find(';')
	exp = eq[id_start+1:id_end]
	str = round_expr(simplify(exp),5).__str__()
	str = str.replace('**','^')
	print(eq[:id_start],'=',str,';',sep='')
